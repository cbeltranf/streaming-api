<?php

namespace Inmovsoftware\StreamingApi\Models\V1;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Streaming extends Model
{
    use SoftDeletes;
    protected $table = "streaming";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['title','iframe','status'];


}
