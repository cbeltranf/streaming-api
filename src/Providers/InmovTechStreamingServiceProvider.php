<?php

namespace Inmovsoftware\StreamingApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\StreamingApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Artisan;

class InmovTechTrainingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'StreamingLangs');

        Artisan::call('vendor:publish' , [
                      '--tag' => 'StreamingLangs',
                    '--force' => true,
        ]);

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'StreamingLangs');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Inmovsoftware\StreamingApi\Models\V1\Streaming');
        $this->app->make('Inmovsoftware\StreamingApi\Http\Controllers\V1\StreamingController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
