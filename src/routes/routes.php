<?php
use Illuminate\Http\Request;

Route::group([
    'middleware' => ['api'],
    'prefix' => 'api/v1'
], function () {
    Route::post('streaming/get', 'Inmovsoftware\StreamingApi\Http\Controllers\V1\StreamingController@get_streaming');
});
